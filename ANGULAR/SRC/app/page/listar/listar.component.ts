import { Component, OnInit } from '@angular/core';
import { switchAll} from 'rxjs';
import { descripcionModel } from 'src/app/models/descripcion.models';
import { marcaModel } from 'src/app/models/marca.models';
import { modeloModel } from 'src/app/models/modelo.models';
import { submarcaModel } from 'src/app/models/submarca.models';
import { ServiceService } from 'src/app/service/service.service';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit{

  marcas: marcaModel[]=[];
  submarcas: submarcaModel[]=[];
  modelos: modeloModel[]=[];
  descripciones: descripcionModel[]=[];
  selectedMarca: any;
  selectedSubmarca: any;
  selectedModelo: any;
  selectedDescripcion: any;
  getSubmarca: any;
  getModelos: any;
  getDescripcion: any;
  getDescripcionId: any;
  url2data: any;

  constructor(private service:ServiceService) {}

  ngOnInit(): void {
    this.service.getCombineData().subscribe(data => {
      this.marcas = data.url1data;
      this.submarcas = data.url2data;
      this.modelos = data.url3data;
      this.descripciones= data.url4data;
    });
  }

  onMarcaChange(){
    if (this.selectedMarca){
      this.submarcas = this.url2data.filter((submarca: { marca: { Marca1: any; }; }) => submarca.marca.Marca1 === this.selectedMarca);
      this.selectedSubmarca = '';
      this.selectedModelo = '';
      this.selectedDescripcion = '';
    } else {
      this.submarcas = [];
      this.selectedSubmarca = '';
      this.selectedModelo = '';
      this.selectedDescripcion = '';
    }
  }

  onSubmarcaChange() {
    if(this.selectedSubmarca) {
      this.modelos = this.url2data.filter((submarca: { submarca: any; }) => submarca.submarca === this.selectedSubmarca).map((submarca: { modelos: any; }) => submarca.modelos).reduce((acc: string | any[], val: any) => acc.concat(val),[]);
      this.selectedModelo = '';
      this.selectedDescripcion = '';
    }else{
      this.modelos = [];
      this.selectedModelo = '';
      this.selectedDescripcion = '';
    }
  }

  onModeloChange(){
    if(this.selectedModelo) {
      this.descripciones = this.url2data
      .filter((submarca: { submarca: any; }) => submarca.submarca === this.selectedSubmarca)
      .map((submarca: { modelos: any[]; }) => submarca.modelos.find(modelo => modelo.modelo === this.selectedModelo)?.descripciones || [])
      .reduce((acc: string | any[], val: any) => acc.concat(val), []);
      this.selectedDescripcion = '';
    }else{
      this.descripciones = [];
      this.selectedDescripcion = '';
    }
  }


}
