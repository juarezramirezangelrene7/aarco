﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AARCO.Models
{
    public partial class AARCOContext : DbContext
    {
        public AARCOContext()
        {
        }

        public AARCOContext(DbContextOptions<AARCOContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Descripcion> Descripcions { get; set; } = null!;
        public virtual DbSet<Marca> Marcas { get; set; } = null!;
        public virtual DbSet<Modelo> Modelos { get; set; } = null!;
        public virtual DbSet<Submarca> Submarcas { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-6H5LGMP\\SQLEXPRESS;DataBase=AARCO;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Descripcion>(entity =>
            {
                entity.ToTable("Descripcion");

                entity.Property(e => e.Descripcion1)
                    .HasMaxLength(100)
                    .HasColumnName("Descripcion");

                entity.Property(e => e.DescripcionId).HasMaxLength(50);
            });

            modelBuilder.Entity<Marca>(entity =>
            {
                entity.ToTable("Marca");

                entity.Property(e => e.Marca1)
                    .HasMaxLength(50)
                    .HasColumnName("Marca");
            });

            modelBuilder.Entity<Modelo>(entity =>
            {
                entity.ToTable("Modelo");

                entity.Property(e => e.Modelo1).HasColumnName("Modelo");
            });

            modelBuilder.Entity<Submarca>(entity =>
            {
                entity.ToTable("Submarca");

                entity.Property(e => e.Submarca1)
                    .HasMaxLength(50)
                    .HasColumnName("Submarca");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
