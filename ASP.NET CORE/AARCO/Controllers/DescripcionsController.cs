﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AARCO.Models;
using System.Threading.Tasks;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace AARCO.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DescripcionesController : ControllerBase
    {
        private readonly AARCOContext _context;

        public DescripcionesController(AARCOContext context)
        {
            _context = context;
        }

        // GET: api/<TarjetaCreditoesController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Descripcion>>> GetDescripcion()
        {
            return await _context.Descripcions.ToListAsync();
        }

        // GET api/<TarjetaCreditoesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Descripcion>> GetDescripcion(int id)
        {
            var descripcion = await _context.Descripcions.FindAsync(id);

            if (descripcion == null)
            {
                return NotFound();
            }
            return descripcion;
        }

        // POST api/<TarjetaCreditoesController>
        [HttpPost]
        public async Task<ActionResult<Descripcion>> PostDescripcion(Descripcion descripcion)
        {
            _context.Descripcions.Add(descripcion);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDescripcion", new { id = descripcion.Id }, descripcion);
        }

        // DELETE api/<TarjetaCreditoesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Descripcion>> DeleteDescricpcion(int id)
        {
            var Descripcion = await _context.Descripcions.FindAsync(id);
            if (Descripcion == null)
            {
                return NotFound();
            }

            _context.Descripcions.Remove(Descripcion);
            await _context.SaveChangesAsync();

            return Descripcion;
        }

        // PUT api/<TarjetaCreditoesController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDescripcion(int id, Descripcion descripcion)
        {
            if (id != descripcion.Id)
            {
                return BadRequest();
            }

            _context.Entry(descripcion).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)

            {
                if (!DescripcionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        private bool DescripcionExists(int id)
        {
            return _context.Descripcions.Any(e => e.Id == id);
        }
    }
}
