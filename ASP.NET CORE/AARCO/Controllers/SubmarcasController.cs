﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AARCO.Models;
using System.Threading.Tasks;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace AARCO.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubmarcasController : ControllerBase
    {
        private readonly AARCOContext _context;

        public SubmarcasController(AARCOContext context)
        {
            _context = context;
        }


        // GET: api/<TarjetaCreditoesController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Submarca>>> GetSubmarca()
        {
            return await _context.Submarcas.ToListAsync();
        }

        // GET api/<TarjetaCreditoesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Submarca>> GetSubmarca(int id)
        {
            var Submarca = await _context.Submarcas.FindAsync(id);

            if (Submarca == null)
            {
                return NotFound();
            }
            return Submarca;
        }

        // POST api/<TarjetaCreditoesController>
        [HttpPost]
        public async Task<ActionResult<Submarca>> PostSubmarca(Submarca submarca)
        {
            _context.Submarcas.Add(submarca);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubmarca", new { id = submarca.Id }, submarca);
        }

        // DELETE api/<TarjetaCreditoesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Submarca>> DeleteSubmarca(int id)
        {
            var Submarca = await _context.Submarcas.FindAsync(id);
            if (Submarca == null)
            {
                return NotFound();
            }

            _context.Submarcas.Remove(Submarca);
            await _context.SaveChangesAsync();

            return Submarca;
        }

        // PUT api/<TarjetaCreditoesController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubmarca(int id, Submarca submarca)
        {
            if (id != submarca.Id)
            {
                return BadRequest();
            }

            _context.Entry(submarca).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)

            {
                if (!SubmarcaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        private bool SubmarcaExists(int id)
        {
            return _context.Submarcas.Any(e => e.Id == id);
        }
    }
}
