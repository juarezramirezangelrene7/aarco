﻿using Microsoft.AspNetCore.Mvc;
using AARCO.Models;
using System.Threading.Tasks;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AARCO.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarcasController : ControllerBase
    {
        private readonly AARCOContext _context;
        public MarcasController(AARCOContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<Marca>>> GetMarcas()
        {
            var connection = _context.Database.GetDbConnection();
            var command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "Marcas_Listar";

            connection.Open();

            using var reader = await command.ExecuteReaderAsync();
            var result = new List<Marca>();

            while (await reader.ReadAsync())
            {
                result.Add(new Marca() { Marca1 = reader.GetString(0) });
            }

            return result;
        }

        //// GET: api/<MarcasController>
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Marca>>> GetMarca()
        //{
        //    return await _context.Marcas.ToListAsync();
        //}

        //// GET api/<TarjetaCreditoesController>/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<Marca>> GetMarca(int id)
        //{
        //    var marca = await _context.Marcas.FindAsync(id);

        //    if (marca == null)
        //    {
        //        return NotFound();
        //    }
        //    return marca;
        //}

        //// POST api/<TarjetaCreditoesController>
        //[HttpPost]
        //public async Task<ActionResult<Marca>> PostMarca(Marca marca)
        //{
        //    _context.Marcas.Add(marca);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetMarca", new { id = marca.Id }, marca);
        //}

        //// DELETE api/<TarjetaCreditoesController>/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Marca>> DeleteMarca(int id)
        //{
        //    var marcas = await _context.Marcas.FindAsync(id);
        //    if (marcas == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Marcas.Remove(marcas);
        //    await _context.SaveChangesAsync();

        //    return marcas;
        //}

        //// PUT api/<TarjetaCreditoesController>/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutMarca(int id, Marca marca)
        //{
        //    if (id != marca.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(marca).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)

        //    {
        //        if (!MarcaExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }
        //    return NoContent();
        //}
        //private bool MarcaExists(int id)
        //{
        //    return _context.Marcas.Any(e => e.Id == id);
        //}
    }
}
