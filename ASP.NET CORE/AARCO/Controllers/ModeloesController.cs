﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AARCO.Models;
using System.Threading.Tasks;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace AARCO.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModeloesController : ControllerBase
    {
        private readonly AARCOContext _context;

        public ModeloesController(AARCOContext context)
        {
            _context = context;
        }


        // GET: api/<TarjetaCreditoesController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Modelo>>> GetModelo()
        {
            return await _context.Modelos.ToListAsync();
        }

        // GET api/<TarjetaCreditoesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Modelo>> GetModelo(int id)
        {
            var Modelo = await _context.Modelos.FindAsync(id);

            if (Modelo == null)
            {
                return NotFound();
            }
            return Modelo;
        }

        // POST api/<TarjetaCreditoesController>
        [HttpPost]
        public async Task<ActionResult<Modelo>> PostModelo(Modelo modelo)
        {
            _context.Modelos.Add(modelo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetModelo", new { id = modelo.Id }, modelo);
        }

        // DELETE api/<TarjetaCreditoesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Modelo>> DeleteModelo(int id)
        {
            var Modelo = await _context.Modelos.FindAsync(id);
            if (Modelo == null)
            {
                return NotFound();
            }

            _context.Modelos.Remove(Modelo);
            await _context.SaveChangesAsync();

            return Modelo;
        }

        // PUT api/<TarjetaCreditoesController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutModelo(int id, Modelo modelo)
        {
            if (id != modelo.Id)
            {
                return BadRequest();
            }

            _context.Entry(modelo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)

            {
                if (!ModeloExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        private bool ModeloExists(int id)
        {
            return _context.Modelos.Any(e => e.Id == id);
        }
    }
}
